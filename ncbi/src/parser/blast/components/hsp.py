#  hsp.py
#
#  Copyright 2017 The University of Sydney
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#
#  Version: 0

#import uuid
import hashlib

class Hsp:

  def __init__(self, blast_hsp, query, hit):
    self.bitscore =  blast_hsp.pop('bit_score', 0)
    self.num = blast_hsp.pop('num', 0)
    self.score = blast_hsp.pop('score', 0)
    self.evalue = blast_hsp.pop('evalue', 100)
    self.identify = blast_hsp.pop('identity', 0)
    self.positive = blast_hsp.pop('positive', 0)
    self.alength = blast_hsp.pop('align_len', 0)
    self.gaps = blast_hsp.pop('gaps', 0)
    self.qbeg = blast_hsp.pop('query_from', 0)
    self.qend = blast_hsp.pop('query_to', 0)
    self.hbeg = blast_hsp.pop('hit_from', 0)
    self.hend = blast_hsp.pop('hit_to', 0)
    #self.uid = str(uuid.uuid4())
    self.uid = hashlib.sha256()
    self.uid.update(query.uid.digest()+hit.uid.digest())
    self.query = query
    self.hit = hit

  def get_uid(self):
    return self.uid.hexdigest()
