# \file srautils.py
# \author Jan Piotr Buchmann <jan.buchmann@mykolab.com>
# \copyright 2018
# \version 0.0.1
# \description Helper struct-like Python classes facilitating SRA managment in
#              blib.
#-------------------------------------------------------------------------------
import hashlib

class BioProject:

  def __init__(self):
    self.uid = 0
    self.accession = None

class SraOrganism:

  def __init__(self):
    self.taxid = 0
    self.name = None

class SraPlatform:

  def __init__(self):
    self.name = None
    self.model = None
    self.uid = None

  def create_uid(self):
    self.uid = hashlib.md5(self.name.encode()+self.model.encode()).hexdigest()
