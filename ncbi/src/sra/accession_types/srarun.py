#-------------------------------------------------------------------------------
# \file srarun.py
# \author Jan Piotr Buchmann <jan.buchmann@mykolab.com>
# \copyright 2018
# \version 0.0.1
# \description Class implementing the SRA Run accession type (SRR)[0].
#               SRR (SRA run accession):
#                 A Run is an object that contains actual sequencing data for a
#                 particular sequencing experiment. Experiments may contain
#                 many Runs depending on the number of sequencing instrument
#                 runs that were needed.
# References:
# [0]: https://www.ncbi.nlm.nih.gov/books/NBK56913/#search.what_do_the_different_sra_accessi
#-------------------------------------------------------------------------------

class SraRun:

  def __init__(self):
    self.accession = None
    self.srx = None # Links to Srx
    self.spots = None
    self.spots_with_mates = None
    self.bases = None
    self.avg_length = None
    self.size_mb = None
    self.path = None
    self.consent = None


  def new(self):
    return SraRun()
