#-------------------------------------------------------------------------------
# \file sraexperiment.py
# \author Jan Piotr Buchmann <jan.buchmann@mykolab.com>
# \copyright 2018
# \version 0.0.1
# \description Class implementing the SRA Experiment accession type (SRX)[0].
#               SRX (SRA experiment accession):
#                 An Experiment is an object that contains the metadata
#                 describing the library, platform selection, and processing
#                 parameters involved in a particular sequencing experiment.
# References:
# [0]: https://www.ncbi.nlm.nih.gov/books/NBK56913/#search.what_do_the_different_sra_accessi
#-------------------------------------------------------------------------------

class SraExperiment:

  def __init__(self):
    self.accession = None
    self.srrs = {}  #Reference map of SraRun()
    self.srps = {}  #Reference map of SraStudy()
    self.srs = None
    self.strategy = None
    self.selection = None
    self.source = None
    self.layout = None
    self.insert_size = 0
    self.insert_dev = 0.0
    self.platform = None  #References SraPlatform()
    self.centername = None

  def link(self, srp, srr, srs, platform):
    self.srrs[srr.accession] = srr
    srr.srx = self
    self.srps[srp.accession] = srp
    srp.srx[self.accession] = self
    self.srs = srs
    srs.srx[self.accession] = self
    self.platform = platform

  def new(self):
    return SraExperiment()
