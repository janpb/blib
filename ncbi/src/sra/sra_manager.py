#-------------------------------------------------------------------------------
#   \file sra_manager.py
#   \author Jan Piotr Buchmann <jan.buchmann@mykolab.com>
#   \copyright 2018
#   \version 0.0.1
#   \description SraManager handles fetched SRA datasets. The SRA data is stored
#                in static dictionaries, one dictionary for each SRA accession
#                type ([0], see below). The population of the static
#                dictionaries is the responsibility of by a separate browser,
#                e.g. sra_run_info_parser.py.
#                Due to the strucure of the SRA data, several types link
#                to each other, e.g. SRX and SRR.
# From [0]:
#  There are 6 different SRA accession types:
#  Accession Prefix Accession Name  Definition  Example
#  SRA: SRA submission accession
#       The submission accession represents a virtual container that holds the
#       objects represented by the other five accessions and is used to track
#       the submission in the archive. Since the SRA accession number is an
#       artificial packaging construct, there is no example available since the
#       SRA accession number has no specific response page
#  SRP: SRA study accession
#       A Study is an object that contains the project metadata describing a
#       sequencing study or project. Imported from BioProject.
#  SRX: SRA experiment accession
#       An Experiment is an object that contains the metadata describing the
#       library, platform selection, and processing parameters involved in a
#       particular sequencing experiment.
#  SRR: SRA run accession
#       A Run is an object that contains actual sequencing data for a
#       particular sequencing experiment. Experiments may contain many Runs
#       depending on the number of sequencing instrument runs that were needed.
#  SRS: SRA sample accession
#       A Sample is an object that contains the metadata describing the
#       physical sample upon which a sequencing experiment was performed.
#       Imported from BioSample.
#  SRZ: SRA analysis accession
#       An analysis is an object that contains a sequence data analysis BAM file
#       and the metadata describing the sequence analysis.
#
#       The first letter of the accession prefix shows which INSDC archive the
#       data originated from (S =NCBI-SRA, E = EMBL-SRA, D = DDBJ-SRA
# References:
# [0]: https://www.ncbi.nlm.nih.gov/books/NBK56913/#search.each_srx_entry_in_the_entrez_sra
#-------------------------------------------------------------------------------
import sys

class SraManager:

  studies = {}
  experiments = {}
  sraruns = {}
  samples = {}
  organisms = {}
  platforms = {}
  bioprojects = {}

  class SraRunManager:

    def __init__(self):
      pass

    def sort(self, column, reverse):
      if column == 'spots':
        return sorted(SraManager.sraruns.values(), key=lambda x: x.spots, reverse=reverse)
      if column == 'bases':
        return sorted(SraManager.sraruns.values(), key=lambda x: x.bases, reverse=reverse)
      if column == 'filesize':
        return sorted(SraManager.sraruns.values(), key=lambda x: x.size_mb, reverse=reverse)
      if column == 'avglen':
        return sorted(SraManager.sraruns.values(), key=lambda x: x.avg_length, reverse=reverse)

    def order_by_organism(self, sortby=None, limit=0):
      runs_per_organism = {}
      if sortby != None:
        reverse = False if limit < 0 else True
        for i in self.sort(sortby, reverse):
          if i.srx.srs.organism.taxid not in runs_per_organism:
            runs_per_organism[i.srx.srs.organism.taxid] = []
          if limit == 0:
            runs_per_organism[i.srx.srs.organism.taxid].append(i)
          else:
            if len(runs_per_organism[i.srx.srs.organism.taxid]) < abs(limit):
              runs_per_organism[i.srx.srs.organism.taxid].append(i)
        return runs_per_organism

      for i in SraManager.sraruns:
        if i.srx.srs.organism.taxid not in runs_per_organism:
          runs_per_organism[i.srx.srs.organism.taxid] = []
        if limit == 0:
          runs_per_organism[i.srx.srs.organism.taxid].append(i)
        else:
          if len(runs_per_organism[i.srx.srs.organism.taxid]) < abs(limit):
            runs_per_organism[i.srx.srs.organism.taxid].append(i)
      return runs_per_organism

  def __init__(self):
    self.runs = self.SraRunManager()

  def dedup(self, sradict, sratype):
    if sratype.accession not in sradict:
      sradict[sratype.accession] = sratype
    return sradict[sratype.accession]

  def get_runs_by_organims(self, sortby=None, limit=0):
    rundict = self.runs.order_by_organism(sortby, limit)
    for i in rundict:
      for j in rundict[i]:
        print(SraManager.organisms[i].taxid, SraManager.organisms[i].name, end='::')
        print(j.accession, j.srx.accession, j.spots, j.bases, j.avg_length,
              j.size_mb, j.path, j.consent)


