#-------------------------------------------------------------------------------
#  \file untitled.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.1
#  \description Class to handle NCBI pubmed article metadata
#-------------------------------------------------------------------------------

class NcbiPubmedArticle:

  class Pubdate:

    def __init__(self, year=None, month=None, day=None):
      self.year = year
      self.month = month
      self.day = day

  class Journal:

    def __init__(self):
      self.title = None
      self.iso_abbrev = None
      self.volume = 0
      self.issue = 0
      self.pages = None
      self.issn = None
      self.pubdate = NcbiPubmedArticle.Pubdate()

  class Medline:

    def __init__(self):
      self.country = None
      self.journal = None
      self.nlnuid = None
      self.issn = None

  class Author:

    def __init__(self):
      self.name = None
      self.lastname = None
      self.initials = None
      self.affiliations = []

  def __init__(self):
    self.pmid = None
    self.pubtype = None
    self.journal = self.Journal()
    self.doi = None
    self.title = None
    self.abstract = None
  #  self.affiliations = {}
    self.authors = []
    self.investigators = []
    self.nlmuid = None
    self.issn = None
    self.essn = None
    self.pubtype = None
    self.article_ids = []
    self.pmcrefcount = 0
    self.fullhournalname = None
    self.doctype = None
