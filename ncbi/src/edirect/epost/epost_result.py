#  -------------------------------------------------------------------------------
#  \file epost_result.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#  -------------------------------------------------------------------------------


import io
import os
import sys


from ..edbase import edresult

class EpostResult(edresult.EdResult):

  def __init__(self, rid, db=None, webenv=None, querykey=None, size=0):
    super().__init__(rid)
    self.db = db
    self.webenv = webenv
    self.querykey = querykey
    self.size = size

  def dump(self):
    print(self.webenv, self.querykey, file=sys.stderr)

  def get_link_parameters(self):
    return {'WebEnv' : self.webenv,
            'QueryKey' : self.querykey,
            'db' : self.db,
            'size' : self.size}
