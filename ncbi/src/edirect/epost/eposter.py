#  -------------------------------------------------------------------------------
#  \file eposter.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#  -------------------------------------------------------------------------------

import sys

from ..edbase import edfunction
from . import epost_request
from . import epost_analyzer

class Eposter(edfunction.EdFunction):

  class Parameters:

    def __init__(self, parameters):
      self.db = parameters.pop('db', None)
      self.uids = parameters.pop('id', None)
      self.webenv = parameters.pop('WebEnv', None)
      self.retmode = parameters.pop('retmode','xml')
      self.check()

    def check(self):
      if self.db == None:
        sys.exit("Efetch error::Missing parameter: no database name (db). Abort.")

  def __init__(self, tool, email, apikey=None):
    super().__init__('epost.fcgi', tool, email, apikey)

  def get_parameters(self, parameters):
    if parameters == None:
      sys.exit("Epost error::Missing req parameter: no options given. Abort.")
    return self.Parameters(parameters)


  def post(self, parameters=None, analyzer=epost_analyzer.EpostAnalyzer()):
    params = self.get_parameters(parameters)
    self.requests.append(epost_request.EpostRequest(0, params))
    self.fetch_requests(analyzer)
    print('\r\n', end='', file=sys.stderr)
