#  -------------------------------------------------------------------------------
#  \file epost_analyzer.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#  -------------------------------------------------------------------------------


import io
import os
import sys
import xml.etree.ElementTree

from . import epost_result
from ..edbase import edanalyzer

class EpostAnalyzer(edanalyzer.EdAnalyzer):

  def __init__(self):
    super().__init__()
    self.result = None

  def analyze_result(self, response, request):
    self.result = epost_result.EpostResult(request.id, db=request.db, size=request.size)
    for event, elem in xml.etree.ElementTree.iterparse(response, events=["end"]):
      if event == 'end' and elem.tag == 'QueryKey':
        self.result.querykey = elem.text
      if event == 'end' and elem.tag == 'WebEnv':
        self.result.webenv = elem.text

  def analyze_error(self, response, request):
    print(response.getvalue())

  def get_result(self):
    return self.result
