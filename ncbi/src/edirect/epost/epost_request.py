#  -------------------------------------------------------------------------------
#  \file epost_request.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#  -------------------------------------------------------------------------------

from ..edbase import edrequest

class EpostRequest(edrequest.EdRequest):

  def __init__(self, request_id, parameters):
    super().__init__('epost', request_id)
    self.db = parameters.db
    self.size = len(parameters.uids)
    self.uids =  ','.join(str(x) for x in parameters.uids)
    self.retmode = parameters.retmode
    self.webenv = parameters.webenv

  def prepare_qry(self):
    qry = self.prepare_base_qry()
    qry.update({'id' : self.uids, 'db' : self.db, 'retmode' : self.retmode})
    if self.webenv != None:
      qry.update({'WebEnv' : self.webenv})
    return qry
