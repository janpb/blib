#-------------------------------------------------------------------------------
#  \file elink_request.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.1
#  \description Elink request for NCBI's EDirect
#-------------------------------------------------------------------------------
from ..edbase import edrequest

class ElinkRequest(edrequest.EdRequest):

  def __init__(self, rid, parameters):
    super().__init__('elink', rid)
    self.source_db = parameters.source_db
    self.target_db = parameters.target_db
    self.cmd = parameters.cmd
    self.querykey = parameters.querykey
    self.webenv = parameters.webenv
    self.uids = parameters.uids
    self.retmode = parameters.retmode
    self.linkname = parameters.linkname
    self.term = parameters.term
    self.holding = parameters.holding
    self.datetype = parameters.datetype
    self.reldate = parameters.reldate
    self.mindate = parameters.mindate
    self.maxdate = parameters.maxdate

  def prepare_qry(self):
    qry = self.prepare_base_qry()
    qry.update({'db' : self.target_db, 'dbfrom' : self.source_db,
                'cmd' : self.cmd, 'retmode' : self.retmode})
    if self.uids != None:
      qry.update({'id' : self.uids})
    else:
      qry.update({'webenv' : self.webenv, 'query_key' : self.querykey})
    if self.linkname != None and (self.cmd == 'neighbor' or self.cmd == 'neighbor_history'):
      qry.update({'linkname' : self.linkname})
    if self.term != None:
      qry.update({'term' : self.term})
    if self.holding != None:
      qry.update({'holding' : self.holding})
    if self.datetype != None:
      qry.update({'datetype' : self.datetype})
    if self.reldate != None:
      qry.update({'reldate' : self.reldate})
    if self.mindate != None:
      qry.update({'mindate' : self.mindate})
    if self.maxdate != None:
      qry.update({'maxdate' : self.maxdate})
    return qry
