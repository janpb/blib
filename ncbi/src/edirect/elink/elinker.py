#-------------------------------------------------------------------------------
#  \file elinker.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright  GNU Lesser General Public License
#  \version 0.0.1-r1
#  \description Elinker class to link EDirect search results.
#-------------------------------------------------------------------------------
from ..edbase import edrequest

import io
import sys
from ..edbase import edfunction
from . import elink_request
from . import elink_analyzer

class Elinker(edfunction.EdFunction):

  class Parameters:

    def __init__(self, options):
      self.target_db = options.pop('db', None)
      self.source_db = options.pop('dbfrom', None)
      self.cmd = options.pop('cmd', 'neighbor')
      self.uids = options.pop('id', None)
      self.querykey = options.pop('query_key', None)
      self.webenv = options.pop('WebEnv', None)
      self.retmode = options.pop('retmode','json')
      self.linkname = options.pop('linkname', None)
      self.term = options.pop('term', None)
      self.holding = options.pop('holding', None)
      self.datetype = options.pop('datetype', None)
      self.reldate = options.pop('reldate', None)
      self.mindate = options.pop('mindate', None)
      self.maxdate = options.pop('maxdate', None)
      self.check()

    def check(self):
      if self.target_db == None:
        sys.exit("Elink error::Missing parameter: no target database (db). Abort.")
      if self.source_db == None:
        sys.exit("Elink error::Missing parameter: no source database (dbfrom). Abort.")
      if self.cmd == None:
        sys.exit("Elink error::Missing parameter: no command mode (cmd). Abort.")

  link_map = {}
  def __init__(self, tool, email, apikey=None):
    super().__init__('elink.fcgi', tool, email, apikey)

  def get_parameters(self, options):
    if options == None:
      sys.exit("Elink error::Missing req parameter: no options given. Abort.")
    return self.Parameters(options)

  def link(self, parameters=None, analyzer=elink_analyzer.ElinkAnalyzer()):
    params = self.get_parameters(options)
    if params.uids != None:
      print("Elinker info: using uids for linking",file=sys.stderr)
    elif params.webenv != None and params.querykey != None:
      print("Elinker info: using history for linking",file=sys.stderr)
    else:
      sys.exit("Elinker error: require uids OR webenv and querykey. Abort")
    self.requests.append(elink_request.ElinkRequest(len(Elinker.link_map), params))
    self.fetch_requests(analyzer)
