#  -------------------------------------------------------------------------------
#  \file elink_result.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#  -------------------------------------------------------------------------------

from ..edbase import edresult

class ElinkResult(edresult.EdResult):

  def __init__(self, rid):
    super().__init__(rid)
    self.database = None
    self.target_db = None
    self.links = []
    self.nohits = []

  def dump(self):
    print(self.database, self.target_db, len(self.links), len(self.nohits), file=sys.stderr)

  def get_link_parameters(self):
    uids = []
    suids = []
    for i in self.links:
      uids += i.target_uids
      suids += i.source_uids
    return {'db' : self.target_db, 'id' : uids}
