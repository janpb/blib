#-------------------------------------------------------------------------------
#  \file elink_analyzer.py
#  \author Jan Piotr Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 USYD
#  \version 0.0.1.62
#  \description
#  \bug Not sure if ids in resposne can be > 1 or if this is influenced by the
#       query. Currently assuming only one. Check carefully.
#------------------------------------------
from . import elink_result
from ..edbase import edanalyzer

class ElinkAnalyzer(edanalyzer.EdAnalyzer):

  class Link:

    def __init__(self, linksetdata, source_uids):
      self.name = linksetdata.pop('linkname', None)
      self.info = linksetdata.pop('info', None)
      self.source_uids = source_uids
      self.target_uids = linksetdata.pop('links', [])

  def __init__(self):
    super().__init__()
    self.result = None

  def analyze_result(self, response, request):
    sourcedbs = {}
    targetdbs = {}
    self.result = elink_result.ElinkResult(request.id)
    for i in response['linksets']:
      if len(sourcedbs) > 1:
        print("ElinkerAnalyzer dev warning: not expecting more than one sourcedb. Check.", file=sys.stderr)
      if i['dbfrom'] not in sourcedbs:
        sourcedbs[i['dbfrom']] = i['dbfrom']
        self.result.database = i['dbfrom']
      if 'linksetdbs' in i:
        for j in i['linksetdbs']:
          if j['dbto'] not in targetdbs:
            targetdbs[j['dbto']] = 0
            self.result.target_db = j['dbto']
            if len(targetdbs) > 1:
              print("ElinkerAnalyzer dev warning: not expecting more than one targetdb. Check.", file=sys.stderr)
          self.result.links.append(self.Link(j, i['ids']))
      else:
        self.result.nohits += i['ids']

  def get_result(self):
    return self.result
