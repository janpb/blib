#  -------------------------------------------------------------------------------
#  \file callimachus_query.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#  -------------------------------------------------------------------------------


import io
import os
import sys
import uuid

class NcbiCallimachusPinakes:

  qry_count = 0
  queries = {}

  @staticmethod
  def show_queries():
    print(NcbiCallimachusPinakes.qry_count)
    print(NcbiCallimachusPinakes.queries)

  @staticmethod
  def get_query(query_id):
    return NcbiCallimachusPinakes.queries[query_id]

  class SearchQuery:

    def __init__(self, parameters, dependency):
      self.qtype = 'esearch'
      self.qid = str(uuid.uuid4())
      self.parameters = parameters
      self.dependency = dependency

    def resolve_dependency(self, results):
      if self.dependency != None:
        parameters = results[self.dependency].result.get_link_parameters()
        parameters.update(self.parameters)
        self.parameters = parameters

  class LinkQuery:

    def __init__(self, parameters, dependency):
      self.qtype = 'elink'
      self.qid = str(uuid.uuid4())
      self.parameters = parameters
      self.dependency = dependency

    def resolve_dependency(self, results):
      if self.dependency != None:
        parameters = results[self.dependency].result.get_link_parameters()
        parameters['dbfrom'] = parameters['db']
        parameters.update(self.parameters)
        self.parameters = parameters

  class PostQuery:

    def __init__(self, parameters, dependency):
      self.qid = str(uuid.uuid4())
      self.qtype = 'epost'
      self.parameters = {} if parameters == None else parameters
      self.dependency = dependency

    def resolve_dependency(self, results):
      if self.dependency != None:
        parameters = results[self.dependency].result.get_link_parameters()
        parameters.update(self.parameters)
        self.parameters = parameters

  class FetchQuery:

    def __init__(self, parameters, dependency, analyzer):
      self.qid = str(uuid.uuid4())
      self.qtype = 'efetch'
      self.parameters = {} if parameters == None else parameters
      self.analyzer = analyzer
      self.dependency = dependency

    def resolve_dependency(self, results):
      if self.dependency != None:
        parameters = results[self.dependency].result.get_link_parameters()
        parameters.update(self.parameters)
        self.parameters = parameters

  def __init__(self):
    self.queries = []
    self.qrymap = {}
    self.qid = NcbiCallimachusPinakes.qry_count
    NcbiCallimachusPinakes.qry_count += 1

  def new_search(self, parameters=None, dependency=None):
    if parameters == None and dependency == None:
      sys.exit("Callimachus error: search missing required parameters and/or dependencies. Abort.")
    return self.SearchQuery(parameters, dependency)

  def new_link(self, parameters=None, dependency=None):
    if parameters == None and dependency == None:
      sys.exit("Callimachus error: link missing required parameters and/or dependencies. Abort.")
    return self.LinkQuery(parameters, dependency)

  def new_fetch(self, parameters=None, dependency=None, analyzer=None):
    if analyzer == None:
      sys.exit("Callimachus error: fetch requests require an analyzer but none given. Abort.")
    if parameters == None and dependency == None:
      sys.exit("Callimachus error: fetch missing required parameters and/or dependencies. Abort.")
    return self.FetchQuery(parameters, dependency, analyzer)

  def new_post(self, parameters=None, dependency=None):
    if parameters == None and dependency == None:
      sys.exit("Callimachus error: post missing required parameters and/or dependencies. Abort.")
    return self.PostQuery(parameters, dependency)

  def add_query(self, query):
    self.queries.append(query.qid)
    NcbiCallimachusPinakes.queries[query.qid] = query
    return query.qid
