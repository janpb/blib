#-------------------------------------------------------------------------------
#  \file ncbi_callimachus.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.2
#  \description The NcbiCallimachus class facilitates the assembly of Edirect
#                queries.
#-------------------------------------------------------------------------------
import io
import os
import sys

from ..esearch import esearcher
from ..elink import elinker
from ..elink import elink_analyzer
from ..efetch import efetcher
from ..epost import eposter
from ..epost import epost_analyzer

from . import callimachus_search_analyzer
from . import callimachus_pinakes

class NcbiCallimachus:

  results = {}
  def __init__(self, email, apikey=None):
    self.tool = 'ncbiCallimachus'
    self.email = email
    self.apikey = apikey

  def new_query(self):
    return callimachus_pinakes.NcbiCallimachusPinakes()

  def run_query(self, qry):
    for i in qry.queries:
      q = callimachus_pinakes.NcbiCallimachusPinakes().get_query(i)
      q.resolve_dependency(NcbiCallimachus.results)
      if q.qtype == 'esearch':
        NcbiCallimachus.results[q.qid] = self.search(q)
      if q.qtype == 'elink':
        NcbiCallimachus.results[q.qid] = self.link(q)
      if q.qtype == 'efetch':
        NcbiCallimachus.results[q.qid] = self.fetch(q)
      if q.qtype == 'epost':
        NcbiCallimachus.results[q.qid] = self.post(q)
      if not NcbiCallimachus.results[q.qid].isSuccess():
        print("Error in query {}".format(q.qid), file=sys.stderr)
        break
    return NcbiCallimachus.results[i]

  def search(self, search_qry, analyzer=callimachus_search_analyzer.CallimachusSearchAnalyzer()):
    analyzer = analyzer.new()
    esearch = esearcher.Esearcher(self.tool, self.email, self.apikey)
    esearch.search(parameters=search_qry.parameters, analyzer=analyzer)
    return analyzer

  def link(self, link_qry, analyzer=elink_analyzer.ElinkAnalyzer()):
    analyzer = analyzer.new()
    linker = elinker.Elinker(self.tool, self.email, self.apikey)
    linker.link(parameters=link_qry.parameters, analyzer=analyzer)
    return analyzer

  def post(self, post_query, analyzer=epost_analyzer.EpostAnalyzer()):
    analyzer = analyzer.new()
    poster = eposter.Eposter(self.tool, self.email, self.apikey)
    poster.post(parameters=post_query.parameters, analyzer=analyzer)
    return analyzer

  def fetch(self, fetch_query):
    analyzer = fetch_query.analyzer
    fetcher = efetcher.Efetcher(self.tool, self.email, self.apikey)
    fetcher.fetch(parameters=fetch_query.parameters, analyzer=analyzer)
    return analyzer
