#  -------------------------------------------------------------------------------
#  \file callimachus_search_analyzer.py
#  \author Jan Piotr Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 USYD
#  \version 0.0.1
#  \description
#  -------------------------------------------------------------------------------

import sys
from .. edbase import edanalyzer
from .. esearch import esearch_result

class CallimachusSearchAnalyzer(edanalyzer.EdAnalyzer):

  def __init__(self):
    super().__init__()
    self.result = None

  def analyze_result(self, response, request):
    self.result = esearch_result.EsearchResult(request.id,
                                               db=request.db,
                                               count=int(response['esearchresult'].pop('count', None)),
                                               webenv=response['esearchresult'].pop('webenv', None),
                                               querykey=int(response['esearchresult'].pop('querykey', None)),
                                               retmax=int(response['esearchresult'].pop('retmax', 0)),
                                               retstart=int(response['esearchresult'].pop('retstart', 0)),
                                               uids=response['esearchresult'].pop('idlist', None))

  #def get_result(self):
  #  return self.result

  def analyze_error(self, response, request):
    print("Error in esearch query of request {}: {}".format(request.id, response['esearchresult']['ERROR']))
    self.result = esearch_result.EsearchResult(request.id, db=request.db)
