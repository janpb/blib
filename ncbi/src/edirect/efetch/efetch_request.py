#-------------------------------------------------------------------------------
#  \file efetch_request.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \description: The EfetchRequest class which inherits edrequest.EdRequest. It
#                assembles Efetch requests and stores the results.
#  \copyright 2017,2018 The University of Sydney
#  \version 1.0.0
#-------------------------------------------------------------------------------
from ..edbase import edrequest

class EfetchRequest(edrequest.EdRequest):

  def __init__(self, request_id, parameters, start, size):
    super().__init__('efetch', request_id)
    self.db = parameters.db
    self.retstart = start
    self.retmax = size
    self.uids = parameters.uids[start:start+size]
    self.webenv = parameters.webenv
    self.querykey = parameters.querykey
    self.webenv = parameters.webenv
    self.querykey = parameters.querykey
    self.rettype = parameters.rettype
    self.retmode = parameters.retmode
    self.strand = parameters.strand
    self.seqstart = parameters.seqstart
    self.seqstop = parameters.seqstop
    self.complexity = parameters.complexity

  def prepare_qry(self):
    qry = self.prepare_base_qry()


    if self.retmode != None:
      qry.update({'retmode' : self.retmode})
    if self.rettype != None:
      qry.update({'rettype' : self.rettype})

    if self.strand != None:
      qry.update({'strand' : self.strand})
    if self.seqstart != None:
      qry.update({'seq_start' : self.parameters.seqstart})
    if self.seqstop != None:
      qry.update({'seq_stop' : self.seqstop})
    if self.complexity != None:
      qry.update({'complexity' : self.complexity})

    if len(self.uids) == 0:
      qry.update({'WebEnv' : self.webenv, 'query_key' : self.querykey})
    else:
      qry.update({'id' : ','.join(str(x) for x in self.uids)})
      self.retstart = 0
      self.retmax = len(self.uids)

    qry.update({'retstart' : self.retstart, 'retmax' : self.retmax})
    return qry

  def dump(self):
    return {'db' : self.db,
            'uids' : len(self.uids),
            'retmax' : self.retmax,
            'retmode' :self.retmode,
            'rettype' : self.rettype,
            'retstart' : self.retstart,
            'webenv' : self.webenv,
            'querykey' : self.querykey}
