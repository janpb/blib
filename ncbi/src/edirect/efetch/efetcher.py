#-------------------------------------------------------------------------------
#  \file efetcher.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \description Implements efetch requests to NCBI. Inherits from EdFunction.
#  \copyright 2018 The University of Sydney
#  \version 1.0.2
#-------------------------------------------------------------------------------
import io
import sys
import math

from ..edbase import edfunction
from ..edbase import edanalyzer
from . import efetch_request

class Efetcher(edfunction.EdFunction):

  request_sizes = {'xml' : 10000, 'json' : 500}

  class Parameters:

    def __init__(self, parameters):
      self.db = parameters.pop('db', None)
      self.uids = parameters.pop('id', [])
      self.webenv = parameters.pop('WebEnv', None)
      self.querykey = parameters.pop('QueryKey', None)
      self.rettype = parameters.pop('rettype', None)
      self.retstart = int(parameters.pop('retstart', 0))
      self.retmax = int(parameters.pop('retmax', 0))
      self.retmode = parameters.pop('retmode', 'xml')
      self.strand = parameters.pop('strand', None)
      self.seqstart = parameters.pop('seq_start', None)
      self.seqstop = parameters.pop('seq_stop', None)
      self.complexity = parameters.pop('complexity', None)
      self.size = parameters.pop('size', len(self.uids)) # not an official NCBI parameter.
      self.check()
      self.query_size = self.set_query_size()

    def check(self):
      if self.db == None:
        sys.exit("Efetch parameter error::Missing parameter: no database name (db). Abort.")
      if self.uids == None and self.webenv == None and self.querykey == None:
        sys.exit("Efetch parameter error::Missing parameter: no ids, QueryKey or WebEnv. Abort.")

    def set_query_size(self):
      if self.retmax > 0:
        return self.retmax - self.retstart
      if self.size > 0:
        return self.size - self.retstart

  def __init__(self, tool, email, apikey=None):
    super().__init__('efetch.fcgi', tool, email, apikey)
    self.query_size = 0
    self.default_request_size = 0

  def set_settings(self, parameters):
    if parameters == None:
      sys.exit("Efetch error::Missing parameters. Abort.")
    p = self.Parameters(parameters)
    self.default_request_size = Efetcher.request_sizes[p.retmode]
    self.query_size = p.query_size
    return p

  def check_settings(self):
    if self.query_size == 0:
      sys.exit("Efetch error::Need to know the size of the query. Abort.")
    if self.default_request_size == 0:
      sys.exit("Efetch error::No default request size given. Something went wrong. Abort.")

  def set_request_size(self):
    if self.query_size < self.default_request_size:
      self.default_request_size = self.query_size
    return self.default_request_size

  def fetch(self, parameters=None, analyzer=edanalyzer.EdAnalyzer()):
    params = self.set_settings(parameters)
    self.check_settings()
    req_size = self.set_request_size()
    expected_requests = math.ceil(self.query_size/req_size)
    print('\t'.join(x for x in ['Reqs', 'Expr', 'req_beg', 'req_size', 'req_end']), file=sys.stderr)
    for i in range(expected_requests):
      if i * req_size + req_size > self.query_size:
        req_size = self.query_size - (i * req_size)
      print(i, expected_requests, (i*self.default_request_size), req_size, (i*self.default_request_size)+req_size, sep='\t', file=sys.stderr)
      self.requests.append(efetch_request.EfetchRequest(i, params,
                                                        (i*self.default_request_size),
                                                        req_size))
    self.fetch_requests(analyzer)
