#  File: esearch_result.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#  Copyright: 2018 The University of Sydney
#  Version: 0

from ..edbase import edresult

class EsearchResult(edresult.EdResult):

  def __init__(self, rid, db=None, count=None, webenv=None, querykey=None, uids=None, retmax=0, retstart=0):
    super().__init__(rid)
    self.db = db
    self.count = count
    self.webenv = webenv
    self.querykey = querykey
    self.retmax = retmax
    self.retstart = retstart
    self.uids = uids
    if self.uids != None and len(self.uids) == 0:
      self.uids = None

  def dump(self):
    print(self.db, self.count, self.webenv, self.querykey, self.retmax, self.retstart, file=sys.stderr)
    if self.uids != None:
      print(len(self.uids), file=sys.stderr)
    else:
      print(self.uids, file=sys.stderr)

  def get_link_parameters(self):
    return {'db' : self.db, 'size' : self.count, 'id' : self.uids,
            'WebEnv' : self.webenv, 'QueryKey' : self.querykey}
