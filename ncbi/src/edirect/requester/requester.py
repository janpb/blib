#  requester.py
#
#  Copyright 2017 The University of Sydney
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>

import sys
import time
import socket
import urllib.parse
import urllib.request
import urllib.error


class Requester:

  def __init__(self, wait):
    self.wait = wait
    self.max_retries = 9
    self.init_timeout = 10
    self.timeout_step = 5
    self.timeout_max = 60

  def request(self, edrequest):
    retries = 0
    success = False
    timeout = self.init_timeout
    while success == False:
      wait = self.wait
      try:
        data = urllib.parse.urlencode(edrequest.prepare_qry(), doseq=True).encode('utf-8')
        edrequest.qry_url = data.decode()
        print(edrequest.url, edrequest.qry_url, file=sys.stderr)
        return urllib.request.urlopen(urllib.request.Request(edrequest.url, data=data),
                                                    timeout=timeout)
      except urllib.error.URLError as url_err:
        print(edrequest.url, edrequest.qry_url, file=sys.stderr)
        print("URL error:", url_err.reason, file=sys.stderr)
        edrequest.request_error = url_err
        if url_err.code == 400: # Bad request form
          edrequest.set_status_fail()
          return None
        retries += 1
        wait = 2
      except urllib.error.HTTPError as http_err:
        print("HTTP error:", httperr.code, http_err.reason, file=sys.stderr)
        if retries > self.max_retries:
          print("Giving up. Retrieving failed after: "+str(retries)+" tries.", file=sys.stderr)
          edrequest.request_error = http_err
          edrequest.set_status_fail()
          return None
        print("Retry {} / {}".format(retries, self.max_retries), file=sys.stderr)
        retries += 1
        wait = 1
      except socket.timeout as timeout_err:
        print("Timeout error {} ({} seconds). Increasing by {} and retry.".format(timeout_err, timeout, self.timeout_step), file=sys.stderr)
        timeout += self.timeout_step
        if timeout > self.timeout_max:
          print("Reached max timeout: {} seconds. Giving up.".format(timeout, self.timeout_step), file=sys.stderr)
          edrequest.set_status_fail()
          return None
      else:
        success = True
        edrequest.set_status_success()
      time.sleep(wait)
