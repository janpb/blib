#-------------------------------------------------------------------------------
#  \file edanalyzer_base.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \description Base class to parse and analyze EDirect results.
#  \copyright 2017,2018 The University of Sydney
#  \version 1.0.2
#  \bug Error handling is wonky
#-------------------------------------------------------------------------------
import io
import sys
import json
import uuid
import xml.etree.ElementTree

class EdAnalyzer:

  @classmethod
  def new(cls):
    return cls()

  def __init__(self):
    self.uid = str(uuid.uuid4())
    self.hasError = False
    self.known_fmts = {'xml':0, 'json':0}

  def parse(self, raw_response, request):
    if request.retmode not in self.known_fmts:
      raise NotImplementedError("Unknown format: {}".format(request.retmode))
    response = self.convert_response(raw_response, request.retmode)
    if self.responseWithError(response, request.retmode):
      self.analyze_error(response, request)
    else:
      self.analyze_result(response, request)

  def analyze_error(self, response, request):
    raise NotImplementedError("Require implementation of analyze_error()")

  def analyze_result(self, response, request):
    raise NotImplementedError("Require implementation of analyze_result()")

  def responseWithError(self, response, fmt):
    if fmt == 'xml':
      self.hasError = self.check_error_xml(response)
      response.seek(0)
    if fmt == 'json':
      self.hasError = self.check_error_json(response)
    return self.hasError

  def check_error_xml(self, response):
    for event, elem in xml.etree.ElementTree.iterparse(response, events=["end"]):
      if elem.tag == 'ERROR':
        return True
    return False

  def check_error_json(self, response):
    if response['header']['type'] == 'esearch' and 'ERROR' in response['esearchresult']:
      return True
    elif response['header']['type'] == 'elink' and 'ERROR' in response:
      return True
    return False

  def convert_response(self, raw_response, fmt):
    response = io.StringIO(raw_response.read().decode())
    if fmt == 'json':
      return json.load(response)
    response.seek(0)
    return response

  def isSuccess(self):
    if self.hasError:
      return False
    return True
