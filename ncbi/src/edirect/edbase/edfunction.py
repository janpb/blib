#-------------------------------------------------------------------------------
#  \file edfunction.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2017,2018 The University of Sydney
#  description  The base class for NCBI requests to Edirect. The specific
#               function, e.g. search or fetch, need to be implemented in the
#                child class
#  \version 1.0.2
#-------------------------------------------------------------------------------
import os
import io
import sys
import math
import time
import collections

from ..requester import requester

class EdFunction:

  def __init__(self, resturl, tool, email, apikey=None):
    self.base_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils"
    self.url = self.base_url + '/' + resturl
    self.contact = email
    self.tool = tool
    self.requests_per_sec = 3       # Default, w/o apikey
    self.max_requests_per_sec = 10  # with apikey
    self.apikey = self.check_ncbi_apikey(apikey)
    self.wait = 1 / self.requests_per_sec
    self.max_retries = 9
    self.request_size = 0
    self.requests = collections.deque()
    self.max_request_size = 5000
    self.requester = requester.Requester(self.wait)

  def check_ncbi_apikey(self, apikey):
    if 'NCBI_API_KEY' in os.environ:
      self.requests_per_sec = self.max_requests_per_sec
      return os.environ['NCBI_API_KEY']
    if apikey != None:
      self.requests_per_sec = self.max_requests_per_sec
      if apikey in os.environ:
        return os.environ[apikey]
      return apikey
    return apikey

  def fetch_requests(self, analyzer):
    request_num = len(self.requests)
    while request_num > 0:
      req = self.requests.popleft()
      self.update_request_essentials(req)
      self.show_progress(req)
      response = self.requester.request(req)
      self.show_progress(req)
      if response != None:
        analyzer.parse(response, req)
      else:
        self.requests.append(req)
      request_num -= 1
      print('\n', file=sys.stderr)

  def update_request_essentials(self, request):
    request.contact = self.contact
    request.url = self.url
    request.tool = self.tool
    if self.apikey != None:
      request.apikey = self.apikey

  def show_progress(self, request):
    print("\r{}\t{}\t{}\t{}\t{}\t{}\n".format(request.typ, request.id, request.status,
                                    request.size, request.url, request.qry_url), end='', file=sys.stderr)
  #"""
    #Since I forgot regularly why using only the 0th index of some request: It's
    #always one search request to get the webkey and webenv for the follow ups.
    #Each is one follow up request. Therefore, no need to loop, just grab the
    #first. Keeps the  requests array intact and will(?) therfore allow sharing
    #hosue keeping routines  with other request.
  #"""
  #def fetch_retstart_request(self, parser):
    #self.show_progress(self.requests[0])
    #self.requester.request(self.requests[0])
    #parser.parse(self.requests[0])
    #self.total_fetched_uids += parser.fetched_uids
    #if self.requests[0].id == 0:
      #if self.limit == 0:
        #self.limit = parser.count
      #self.expected_uids = self.limit
      #self.expected_batches = math.ceil(self.expected_uids / parser.retmax)
    #if self.retstart_isComplete(self.requests[0], parser) == True:
      #self.show_progress(self.requests[0], status=1)
    #else:
      #self.show_progress(self.requests[0], status=2)
