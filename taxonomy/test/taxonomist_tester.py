#!/usr/bin/env python3
#-------------------------------------------------------------------------------
#  File: taxonomist_tester.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Copyright: 2018 The University of Sydney
#  Version: 0
#  Description:
#-------------------------------------------------------------------------------


import io
import os
import sys
import random
sys.path.insert(1, '../taxonomy/src')

import taxonomist

class SimpleTaxonomist(taxonomist.NcbiTaxonomist):

  def __init__(self):
    super().__init__()


def main():

  fh = open(sys.argv[1], 'r')
  st = SimpleTaxonomist()
  queries = st.parse(fh)
  st.write_local_database('taxonomist.local.db')
  #taxid_to_del = random.choice(list(st.taxa))
  #taxid_to_del = 562
  #del_taxid = st.taxa.pop(taxid_to_del)
  #print("Deleted {}".format(del_taxid.taxid))
  #print(del_taxid.dump())
  st.read_local_database('taxonomist.local.db')
  fh.close()

  for i in queries:
    lin = st.assemble_lineage_from_taxid(i)
    #for j in lin:
      #print(j.dump())
    clade = st.get_clade_from_lineage(lin)
    for j in clade.get_normalized_lineage(lin):
      print(j.name, j.rank)
    print("---------------------")


  #t = st.get_taxa_table()
  #for i in queries:
    #for i in t.get_lineage(i, normalized=True):
      #print(i)
    #print("------------")
  #st.store_query_lineages(queries)
  #st.dump()

if __name__ == '__main__':
  main()
