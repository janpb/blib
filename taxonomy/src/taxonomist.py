#-------------------------------------------------------------------------------
# \file taxonomy.py
# \author: Jan P Buchmann <lejosh@members.fsf.org>
# \date 2018
# \copyright  GNU Lesser General Public License
# \version 1.0.1
# \bug Handling the query taxid's is not yet fully established
# \brief NCBI Taxonomist parses and analyzes NCBI taxonomies.
# \details  The class parses the NCBI taxonomy XML files. Based on the NCBI
#           taxid it can assemble lineages and group several lineages into
#           clades.  Lineages consist of taxons. Each parsed taxon is stores in
#           a static database and  knows its parent and children. This allows
#           to assembles lineages on the fly.
#
#           Parsing:
#           Parsing of Taxonomical data is done in specific parsers, so far only
#           XML, and are expected to return a dictionary of queries, i.e. the
#           taxon whoch was used in a lookuop. This is requires for Edirect
#           requests.
#
#           Databases:
#           Taxonomic data cae be stored locally in a sqlite3 database. Three
#           basic tables are expected:
#             - taxid: taxid, rank, name, parent taxid
#             - onym: taxid, synonym, acronym
#             - aliases: taxid, alias (alias is the proper taxid for taxid)
#
#           Lineages:
#           Lineages are a list of taxons linked by their parent. These lists
#           can be grouped into clades, e.g. eukaryote or virus. Clades are
#           specific classes and offer functions dealeing with lineages within
#           a clade.
#-------------------------------------------------------------------------------
import os
import sys
import json

import taxoparser.ncbi_xml_parser
import taxon.basic_taxon
import database.taxonomy_db
import cladistic

## NcbiTaxonomist
# @ingroup taxonomist
# NcbiTaxonomist is the main class to deal with NCBI taxonomies. It stores
# all encountered taxons in a static dictionary. It is considered as a
# "singleton", i.e. only one Taxonomist should be instantiated for an analysis.
class NcbiTaxonomist:
  ## Taxa database
  # Static dictionary for all taxon objects
  taxa = {}
  aliases = {}

  ## Database handling
  # A static instance of database.taxonomy_db.TaxonomyDb()
  database = database.taxonomy_db.TaxonomyDb()

  ## Clade handling
  # A static instance of cladistic.Cladistic()
  cladistic = cladistic.Cladistic()

  ## Constructor
  def __init__(self):
    pass

  ## Parser selector, returns the appropriate parser for format fmt
  # @param fmt, str, format
  # @return parser, an instance of the corresponding parser
  def set_parser(self, fmt):
    if fmt == 'xml':
      return taxoparser.ncbi_xml_parser.NcbiXmlParser(NcbiTaxonomist.taxa, NcbiTaxonomist.aliases)

  ## Taxonomy parser
  # @param source,file-like object
  # @param format, source format, so far only NCBI Taxonomy XML
  # @return dict, queries
  def parse(self, src, fmt='xml'):
    p = self.set_parser(fmt)
    return p.parse(src)

  ## Function to assemble a lineage from a taxid
  # @param taxid, NCBI_Taxid
  # @return taxa, a list of taxon instances for lineage
  def assemble_lineage_from_taxid(self, taxid):
    taxon = NcbiTaxonomist.taxa[int(taxid)]
    taxa = []
    while taxon.parent_taxid != None:
      taxa.insert(0, taxon)
      taxon = NcbiTaxonomist.taxa[taxon.parent_taxid]
    taxa.insert(0, taxon)
    return taxa

  ## Determine the clade for a lineage
  # The lineage should be assembled as it speeds up the identification
  # @param lineage, list, list of taxons
  # @return Clade instance or None
  def get_clade_from_lineage(self, lineage):
    if len(lineage) == 0:
      return None
    return self.get_clade(self.cladistic.identify_clade(lineage))

  ## Helper function to obtain a specific clade from Cladistic
  # @param cladename, string, clade name
  # @return clade instance or None
  def get_clade(self, cladename):
    if self.cladistic.cladeExists(cladename):
      return self.cladistic.clades[cladename]
    return None

  ## Helper function to obtain a specific clade from Cladistic
  # @param clade, string, clade name
  # @return clade instance or None
  def get_clades(self):
    if len(self.cladistic.clades) > 0:
      return self.cladistic.clades
    return None

  ## Resolve aliases for taxids
  # @param, taxid to resolve
  # @return taxid linking to input taxid or None
  def resolve_alias(self, src_taxid):
    if src_taxid in NcbiTaxonomist.aliases:
      return NcbiTaxonomist.taxa[NcbiTaxonomist.aliases[src_taxid]]
    return None

  ## Store lineages into a SQLite3 database
  # @param clade_names,  list,  name of clades to store. If None, store all
  def store_clades(self, clade_names=None):
    clades = []
    if clade_names == None:
      clades = self.cladistic.get_clades()
    else:
      clades = [self.cladistic.get_clade(i) for i in clade_names]
    if len(clades) == 0:
      sys.exit("No clades found. Prepare clades, see get_clades_from_taxid()")
    for i in clades:
      if not NcbiTaxonomist.database.tableExists(i.name):
        table = database.table.clade_table.CladeTable(i.name, NcbiTaxonomist.database)
        table.create(i)
      if  NcbiTaxonomist.database.get_table(i.name) != None:
        table = NcbiTaxonomist.database.get_table(i.name)
        table.insert(i)

  ## Store query lineages into a SQLite3 database
  # @param queries, query taxids
  def store_query_lineages(self, queries):
    clades = self.cladistic.get_clades()
    if len(clades) == 0:
      for i in queries:
        self.get_clade_from_taxid(i)
      clades = self.cladistic.get_clades()
    for i in clades:
      if not NcbiTaxonomist.database.tableExists(i):
        table = database.table.clade_table.CladeTable(i, NcbiTaxonomist.database)
        table.create(clades[i])
      if  NcbiTaxonomist.database.get_table(i) != None:
        table = NcbiTaxonomist.database.get_table(i)
        table.insert(clades[i])

  ## Return a taxonomy lineage with clade specifiec rank names only
  #  i.e. ommit 'no rank' taxons
  # @param clade, str,  clade name
  # @lineage, lineage, list of taxons
  def get_normalized_clade_lineage(self, cladename, lineage):
    clade = self.get_clade(cladename)
    return clade.get_normalized_lineage(lineage)

  ## Return a taxonomy lineage with taxa with specific ranks
  #  i.e. ommit 'no rank' taxons
  # @lineage, lineage, list of taxons
  def get_normalized_lineage(self, lineage):
    norm = []
    for i in lineage:
      if i.rank != 'no rank':
        norm.append(i)
    return norm

  ## Helper function to dump the taxonomy database
  # @param sink, string, possible filename. Defaults to stdout.
  def dump_database(self, sink=None):
    NcbiTaxonomist.database.dump(sink)

  ## Simple getter for a taxon
  # @param taxid, int, NCBI_taxid
  # @return Taxon instance or None
  def get_taxon(self, taxid):
    if taxid in NcbiTaxonomist.taxa:
      return NcbiTaxonomist.taxa[taxid]
    return None

  ## Simple getter for ranks of a specific clade
  # @param clade, clade, clade name
  # @return ranks,  list, ordered ranks of clade
  def get_clade_ranks(self, clade):
    return NcbiTaxonomist.cladistic.rankmap.get(clade, None)

  ## Store taxonomy on disk as SQlite3
  def write_local_database(self, localdb_path, batch_size=10000):
    localdb = database.taxonomy_db.TaxonomyDb(localdb_path)
    if localdb.isTaxonomyDb():
      localdb.write_sqlite3(NcbiTaxonomist.taxa)

  ## Test if local TaxononomyDB database exists
  # @input database path
  # @return bool, True if does, False if not
  def databaseExists(self, dbpath):
    if os.path.isfile(dbpath):
      print("Database {} exists".format(dbpath), file=sys.stderr)
      return True
    print("Database {} doesn't exists".format(dbpath), file=sys.stderr)
    return False

  ## Cheap test if SQLite3 databse is empty using the file size
  # @input database path
  # @return bool, True if does, False if not
  def databaseIsEmpty(self, dbpath):
    if os.path.getsize(dbpath) == 0:
      print("Database {} is empty".format(dbpath), file=sys.stderr)
      return True
    print("Database {} is not empty".format(dbpath), file=sys.stderr)
    return False

  ## Store taxonomy in memory as SQlite3
  # @return status, int, 0 if no issues with writing sqlite db
  def write_memory_database(self):
    NcbiTaxonomist.database.write_sqlite3(NcbiTaxonomist.taxa)

  ## Initialize empty SQlite3 TaxonomyDb
  # @return localdb, TaxonomyDb instance
  def initialize_local_db(self, localdb_path):
    localdb = database.taxonomy_db.TaxonomyDb(localdb_path)
    localdb.init_tables()
    print("Initialized new TaxonomyDb: {}.".format(localdb.name), file=sys.stderr)
    return localdb

  def connect_local_db(self, localdb_path):
    if self.databaseExists(localdb_path):
      localdb = database.taxonomy_db.TaxonomyDb(localdb_path)
      if not localdb.isTaxonomyDb():
        sys.exit("Error: {} is not valid TaxonomyDb.".format(localdb.name))
      print("Connected to existing and valid TaxonomyDb: {}.".format(localdb.name), file=sys.stderr)
      localdb.init_tables()
      return localdb
    localdb = self.initialize_local_db(localdb_path)
    print("Connected new TaxonomyDb: {}.".format(localdb.name), file=sys.stderr)
    return localdb

  ## Close connection to local db
  #@input localdb instance
  def disconnect_local_db(self, localdb):
    localdb.close_connection()
    return localdb.name

  ## Read local SQlite3 TaxonomyDB
  # Need to check is its possible to use less calls to SQlite3
  def read_local_database(self, localdb):
    taxatbl = localdb.get_taxa_table()
    new_taxids = {}
    for i in taxatbl.get_rows():
      if i['taxid'] not in NcbiTaxonomist.taxa:
        new_taxids[int(i['taxid'])] = 0
        t = taxon.basic_taxon.BasicNcbiTaxon(i['taxid'], i['name'], i['rank'], i['parent_taxid'])
        NcbiTaxonomist.taxa[int(t.taxid)] = t
    if len(new_taxids) > 0:
      syntbl = localdb.get_synonym_table()
      for i in syntbl.get_rows():
        if int(i['taxid']) in new_taxids:
          NcbiTaxonomist.taxa[int(i['taxid'])].synonyms.append(i['synonym'])
      acrotbl = localdb.get_acronym_table()
      for i in acrotbl.get_rows():
        if int(i['taxid']) in new_taxids:
          NcbiTaxonomist.taxa[int(i['taxid'])].acronyms.append(i['acronym'])
      aliastbl = localdb.get_alias_table()
      for i in aliastbl.get_rows():
        if int(i['taxid']) in new_taxids:
          NcbiTaxonomist.taxa[int(i['taxid'])].aliases.append(i['alias'])
