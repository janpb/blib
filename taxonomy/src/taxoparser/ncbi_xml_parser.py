#-------------------------------------------------------------------------------
# \file ncbi_xml_parser.py
# \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
# \copyright 2018 The University of Sydney
# \version 0.1.0
# \description A NCBI XML Taxonomy parser.
#           The first encountered taxon in a NCBI taxonomy XML file is
#           considered a query and stored separately and can be used to handle
#           EDirect requests.
#-------------------------------------------------------------------------------

import os
import sys
import xml.etree.ElementTree as ET

sys.path.insert(1, os.path.join(sys.path[0], '../'))
import taxon.basic_taxon

class NcbiXmlParser:

  def __init__(self, taxamap, aliasmap):
    self.taxamap = taxamap
    self.aliasmap = aliasmap
    self.collect_aliases = False

  ## NCBI taxonomy XML parser
  # @param xml NCBI taxonomy XML, file-like object
  # @param t, Taxon object, Initialized taxon
  # @return dict, queries
  def parse(self, xml, t=taxon.basic_taxon.BasicNcbiTaxon(), qry = taxon.basic_taxon.BasicNcbiTaxon()):
    isLineage = False
    parent = None
    queries = {}
    for event, elem in ET.iterparse(xml, events=["start", "end"]):
      if event == 'start' and elem.tag == 'LineageEx':
        isLineage = True
      if event == 'end' and elem.tag == 'LineageEx':
        isLineage = False
      if not isLineage:
        if elem.tag == 'Taxon' and event == 'start':
          parent = None
          qry = taxon.basic_taxon.BasicNcbiTaxon()
        elif elem.tag == 'Taxon' and event == 'end':
          self.update_phylogeny(qry)
          queries[qry.taxid] = 0
          qry.add_child(t)
        elif elem.tag == 'TaxaSet':
          continue
        else:
          self.parse_taxon(event, elem, qry)

      if isLineage:
        if elem.tag == 'Taxon' and event == 'start':
          t = taxon.basic_taxon.BasicNcbiTaxon()
        elif elem.tag == 'Taxon' and event == 'end':
          if parent != None:
            t.parent_taxid = parent.taxid
            parent.add_child(t)
          parent = self.update_phylogeny(t)
        else:
          self.parse_taxon(event, elem, t)
    return queries

  def update_phylogeny(self, taxon):
    if taxon.taxid not in self.taxamap:
      self.taxamap[taxon.taxid] = taxon
    if taxon.hasAlias():
      self.aliasmap[taxon.get_alias()] = taxon.taxid
      self.taxamap[taxon.get_alias()] = taxon
    return self.taxamap[taxon.taxid]

  ## Method to parse a taxon node in NCBI Taxonomy XML entries
  # Storing required tags to reconstruct the lineage. The values are stored
  # directly in the correspondign taxon.
  # @param event, xml.etree.ElementTree event, 'start' or 'stop'
  # @param elem, xml.etree.ElementTree element, the XML tag
  # @param taxon, blib taxonomy taxon instance, either BasicTaxon or QueryTaxon
  def parse_taxon(self, event, elem, taxon):
    if event == 'start' and elem.tag == 'AkaTaxIds':
      self.collect_aliases = True
    if event == 'end':
      if elem.tag == 'TaxId' and not self.collect_aliases:
        taxon.taxid = int(elem.text)
      if elem.tag == 'TaxId' and self.collect_aliases:
        taxon.add_alias(elem.text)
      if elem.tag == 'ScientificName':
        taxon.name = elem.text
      if elem.tag == 'Synonym':
        taxon.add_synonym(elem.text)
      if elem.tag == 'Acronym':
        taxon.add_acronym(elem.text)
      if elem.tag == 'Rank':
        taxon.rank = elem.text
      if elem.tag == 'ParentTaxId':
        taxon.parent_taxid = int(elem.text)
      if elem.tag == 'Division':
        taxon.division = elem.text
      if elem.tag == 'GCId':
        taxon.genetic_code_id = int(elem.text)
      if elem.tag == 'GCName':
        taxon.genetic_code_name = elem.text
      if elem.tag == 'MGCId':
        taxon.mitogenetic_code_id = int(elem.text)
      if elem.tag == 'MGCName':
        taxon.mitogenetic_code_name = elem.text
      if elem.tag == 'CreateDate':
        taxon.create_date = elem.text
      if elem.tag == 'UpdateDate':
        taxon.update_date = elem.text
      if elem.tag == 'PubDate':
        taxon.pubdate = elem.text
      if elem.tag == 'PubDate':
        taxon.pubdate = elem.text
      if elem.tag == 'AkaTaxIds':
        self.collect_aliases = False
