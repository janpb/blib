#-------------------------------------------------------------------------------
#  \file: taxonomy_db.py
#  \author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright  GNU Lesser General Public License
#  \version: 1.0.1
#  \description SQLite handling for taxonomic data
#-------------------------------------------------------------------------------
import sys
import sqlite3

from .table import ncbi_taxid_table
from .table import ncbi_synonym_table
from .table import ncbi_acronym_table
from .table import ncbi_alias_table

## TaxonomyDb
# @ingroup taxonomist
# TaxonomyDb handles SQLite3 taxonomy databases. Taxonomy databases have so far
# 4 required tables:
# - taxatable: table storing taxids with corresponding name, rank, and
#   parent_taxid
# - acronymtable: table storing taxids and corresponding acronyms
# - synonymtable: table storing taxids and corresponding synonyms
# - aliastable: table storing taxids (old taxid) and corresponding new alias
class TaxonomyDb:

  req_tables = {'taxatable' : 'taxa', 'acronymtable' : 'acronym',
                'synonymtable' : 'synonym','aliastable' : 'alias'}

  ## Constructor
  # Initialize a sqlite3 database in memory
  def __init__(self, name=':memory:'):
    self.name = name
    self.conn = sqlite3.connect(self.name)
    self.conn.row_factory = sqlite3.Row
    self.tables = {}

  ## Initialize required tables for a minimal working Taxonomist SQlite3
  #  database
  def init_tables(self):
    for i in self.req_tables:
      tbl = None
      if i == 'taxatable' and not self.tableExists(TaxonomyDb.req_tables[i]):
        tbl = ncbi_taxid_table.NcbiTaxidTable(TaxonomyDb.req_tables[i], self)
      if i == 'acronymtable' and not self.tableExists(TaxonomyDb.req_tables[i]):
        tbl = ncbi_acronym_table.NcbiAcronymTable(TaxonomyDb.req_tables[i], self)
      if i == 'synonymtable' and not self.tableExists(TaxonomyDb.req_tables[i]):
        tbl = ncbi_synonym_table.NcbiSynonymTable(TaxonomyDb.req_tables[i], self)
      if i == 'aliastable' and not self.tableExists(TaxonomyDb.req_tables[i]):
        tbl = ncbi_alias_table.NcbiAliasTable(TaxonomyDb.req_tables[i], self)
      if tbl != None:
        tbl.create()
        self.tables[tbl.name] = tbl
        tbl.count_rows()
        tbl = None

  ## Getter for required database taxatable
  # @return taxatable, taxatable instance
  def get_taxa_table(self):
    return self.get_table(TaxonomyDb.req_tables['taxatable'])

  ## Getter for required database aliastable
  # @return aliastable, aliastable instance
  def get_alias_table(self):
    return self.get_table(TaxonomyDb.req_tables['aliastable'])

  ## Getter for required database synonym
  # @return synonymtable, synonymtable instance
  def get_synonym_table(self):
    return self.get_table(TaxonomyDb.req_tables['synonymtable'])

  ## Getter for required database acronym
  # @return acronymtable, acronymtable instance
  def get_acronym_table(self):
    return self.get_table(TaxonomyDb.req_tables['acronymtable'])

  ## Getter for database tables
  # @param table_name, str, name of table instance to get
  # @return table, table instance
  # @return None, None, if table doesn't exist
  def get_table(self, table_name):
    if self.tableExists(table_name):
      return self.tables[table_name]
    return None

  ## Check if table exists
  # @param table_name, str, table name
  # @return boolean, True if exists, else False
  def tableExists(self, table_name):
    return table_name in self.tables

  ## Helper function to register tables
  # @param table, table instance
  def register_table(self, table):
    self.tables[table.name] = table

  ## Helper function to execute sqlite3 statements
  # @param stmt, str, SQlite4 statement
  # @params values, statement values
  # @return execution instance
  def execute_stmt(self, stmt, values=None):
    c = self.conn.cursor()
    if values == None:
      return c.execute(stmt)
    return c.execute(stmt, values)

  ## SQlite3 dumper function. Dump database to file or STDOUT. Without filename
  # use STDOUT
  # @param sink, str, filename
  def dump(self, sink=None):
    if sink == None:
      for i in self.conn.iterdump():
        print(i, file=sys.stdout)
    else:
      fh = open(sink, 'w')
      for i in self.conn.iterdump():
        fh.write(i+'\n')
      fh.close()

  ## Store taxonomy as SQlite3 database
  def write_sqlite3(self, taxa, batch_size=100000):
    self.init_tables()
    batch = []
    for i in taxa:
      batch.append(taxa[i])
      if len(batch) % batch_size == 0:
        for i in self.tables:
          tables[i].insert(batch)
        batch = []
    if len(batch) > 0:
      for i in self.tables:
        self.tables[i].insert(batch)
      batch = []

  ## Test if SQlite3 table is valid TaxonomyDb
  #@return bool, True if is, False if not.
  def isTaxonomyDb(self):
    db_tables = self.get_database_tables()
    for i in TaxonomyDb.req_tables:
      if TaxonomyDb.req_tables[i] not in db_tables:
        print("Error: {0} is not a valid TaxonomyDB SQlite3 database.\
                  \n\r\tMissing required table '{1}'.".format(self.name,
                                                              TaxonomyDb.req_tables[i]))
        return False
    return True

  ## Fetch all tables present in a SQlite3 database
  # @return tables, dictionary, existing table names dictionary
  def get_database_tables(self):
    tables = {}
    for i in self.execute_stmt("""SELECT name FROM sqlite_master WHERE type='table'"""):
      tables[i[0]] = 0
    return tables

  ## Close SQLite3 connetion
  #@return 0, int, success
  def close_connection(self):
    self.conn.close()
    return 0
