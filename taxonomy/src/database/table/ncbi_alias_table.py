#!/usr/bin/env python3
#-------------------------------------------------------------------------------
#  \file ncbi_alias_table.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.1.0
#  \description
#-------------------------------------------------------------------------------

from . import taxonomy_table

class NcbiAliasTable(taxonomy_table.TaxonomyTable):

  def __init__(self, name, database):
    super().__init__(name=name, database=database)

  def create(self):
    c = self.database.conn.cursor()
    stmt = """CREATE TABLE IF NOT EXISTS {}
              (
                id           INTEGER PRIMARY KEY,
                taxid        INT,
                alias        INT,
                FOREIGN KEY (alias) REFERENCES taxa(taxid),
                UNIQUE(taxid)
              )""".format(self.name)
    c.execute(stmt)
    return self

  def get_rows(self):
    c = self.database.conn.cursor()
    return c.execute("SELECT taxid, alias FROM {0}".format(self.name))

  def insert(self, taxa):
    stmt = """INSERT OR IGNORE INTO {0} (taxid, alias)
                          VALUES (?,?)""".format(self.name)
    c = self.database.conn.cursor()
    values = []
    for i in taxa:
      if i.hasAlias():
        values.append([i.taxid, i.get_alias()])
    c.executemany(stmt, values)
    self.database.conn.commit()
    self.count_rows()
