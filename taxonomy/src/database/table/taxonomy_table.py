#-------------------------------------------------------------------------------
#  \file taxonomy_table.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright  GNU Lesser General Public License
#  \version 1.0.1
#  \description Basic class for taxonomy SQlite3 tables
#-------------------------------------------------------------------------------

## TaxonomyTable
# @ingroup taxonomist
# TaxonomyTable is the main class to deal with NCBI taxonomy SQlite3 tables.
# Table instances are registered by TaxonomyDb. It acts as very simple base
# class.

class TaxonomyTable:

  ## static rankmap to map taxonokmy names which clash with SQlite3 keywords
  rankmap = {'group' : 'grp', 'order' : 'ordr'}

  ## Constructor
  # @param name, str, clade name
  # @param database, reference, reference to corresponding SQlite3 instance
  def __init__(self, name=None, database=None):
    self.name = name
    self.database = database
    self.size = 0

  ## Creating table
  # A virtual function
  def create(self):
    raise NotImplementedError("Implement create() method")

  ## Getting all rows
  # A virtual function
  def get_rows(self):
    raise NotImplementedError("Help! Implement get_rows() method")

  ## Helper function to commit changes
  #def commit(self):
    #self.database.conn.commit()

  ## Getter function to get static rankmap
  # @returns rankmap, dict
  def get_col_rank(self, rank):
    return self.__class__.rankmap.get(rank, rank)

  def count_rows(self):
    stmt = "SELECT COUNT(id) FROM {0}".format(self.name)
    c = self.database.execute_stmt(stmt)
    self.size = c.fetchone()[0]
    return self.size
