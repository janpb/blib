#  -----------------------------------------------------------------------------
#  File: ncbi_taxid_table.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Copyright: 2018 The University of Sydney
#  Version: 0
#  Description: Table class handling NCBI taxa
#  -----------------------------------------------------------------------------

from . import taxonomy_table

class NcbiTaxidTable(taxonomy_table.TaxonomyTable):

  def __init__(self, name, database):
    super().__init__(name=name, database=database)

  def create(self):
    c = self.database.conn.cursor()
    stmt = """CREATE TABLE IF NOT EXISTS {0}
              (
                id           INTEGER PRIMARY KEY,
                taxid        INT,
                rank         TEXT,
                name         TEXT,
                parent_taxid INT NULL,
                FOREIGN KEY (parent_taxid) REFERENCES taxa(taxid),
                UNIQUE(taxid)
              )""".format(self.name)
    c.execute(stmt)
    return self

  def insert(self, taxa):
    #print("inserting", self.database.name, self.name)
    c = self.database.conn.cursor()
    stmt = """INSERT OR IGNORE INTO {0} (taxid, rank, name, parent_taxid)
                          VALUES (?,?,?,?)""".format(self.name)
    c.executemany(stmt, [(x.taxid, x.rank, x.name, x.parent_taxid) for x in taxa])
    self.database.conn.commit()
    self.count_rows()

  def get_taxids(self):
    c = self.database.conn.cursor()
    return c.execute("""SELECT taxid FROM {}""".format(self.name))

  def get_rows(self):
    c = self.database.conn.cursor()
    return c.execute("SELECT taxid, rank, name, parent_taxid FROM {0}".format(self.name))

  def get_lineage(self, taxid, normalized=False):
    stmt = """WITH RECURSIVE parent(taxid) AS
              (
                SELECT taxid                    \
                FROM {0}                         \
                WHERE taxid=?                  \
                UNION                           \
                SELECT n.parent_taxid           \
                FROM {0} n, parent         \
                WHERE n.taxid=parent.taxid
              )
              SELECT n.taxid,                   \
                     n.name,                    \
                     n.rank,                    \
                     n.parent_taxid             \
              FROM {0} n                   \
              JOIN parent p ON n.taxid=p.taxid  \
              """.format(self.name)
    if normalized:
      stmt += "WHERE n.rank != 'no rank'"
    c = self.database.conn.cursor()
    return c.execute(stmt, (taxid,))
