#-------------------------------------------------------------------------------
#  \file clade_table.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright  GNU Lesser General Public License
#  \version 1.0.1
#  \description Clade table to store taxomoy lineages
#-------------------------------------------------------------------------------
import sys
from. import taxonomy_table

## CladeTable
# @ingroup taxonomist
# CladeTable handles the most common clades of a taxonomy.
# It inherits taxonomy_table.TaxonomyTable
class CladeTable(taxonomy_table.TaxonomyTable):

  ## Constructor
  # @param name, str, clade name
  # @param database, reference, reference to corresponding sqlite3 instance
  def __init__(self, name, database):
    super().__init__(name, database)

  ## Create clade table and register it at the database
  # @param clade, clade instance
  def create(self, clade):
    header = "id INTEGER PRIMARY KEY, qry_taxid INT,qry_name TEXT,qry_rank TEXT"
    for i in clade.ranks:
      header += ", {} TEXT DEFAULT 'NA'".format(self.get_col_rank(i))
    header += ", UNIQUE(qry_taxid)"
    stmt = "CREATE TABLE {} ({}) ".format(clade.name, header)
    c = self.database.conn.cursor()
    c.execute(stmt)
    self.database.register_table(self)

  ## Insert clade
  # @param clade, clade instance
  def insert(self, clade):
    c = self.database.conn.cursor()
    for i in clade.lineages:
      cols = ''
      values = ''
      for j in i:
        if clade.isNormalizedRank(j):
          cols += "{},".format(self.get_col_rank(j.rank))
          values += "'{}',".format(j.name.replace('\'', ''))
      cols = cols[:-1]
      values = values[:-1]
      stmt = """INSERT INTO {} ({}) VALUES ({}) """.format(clade.name, cols, values)
      #print(stmt, file=sys.stderr)
      c.execute(stmt)
    self.commit()
