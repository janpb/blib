#  -----------------------------------------------------------------------------
#  File: clade.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Copyright: 2018 The University of Sydney
#  Version: 1.0.1
#  Description: The base class for clades. Implements methods which can be
#               applied to all clades.
#  -----------------------------------------------------------------------------
import os
import sys

sys.path.insert(1, os.path.join(sys.path[0], '../'))
import taxon.basic_taxon

class BasicClade:

  def __init__(self, name, ranklist):
    self.ranks = {ranklist[x] : x for x in range(len(ranklist))}
    self.ranklist = ranklist
    self.name = name

  def isNormalizedRank(self, taxon):
    return taxon.rank in self.ranks

  def normalize_lineage(self, taxa):
    norm = [taxon.basic_taxon.BasicNcbiTaxon() for x in range(len(self.ranks))]
    #seen = {**self.ranks} # PEP 448, >= Python 3.5
    seen = dict(self.ranks) # < Python 3.5
    for i in taxa:
      if self.isNormalizedRank(i):
        norm[self.ranks[i.rank]] = i
        seen.pop(i.rank)
    for i in seen:
      norm[seen[i]].rank = i
      norm[seen[i]].name = 'NA'
    return norm

  def get_normalized_lineage(self, taxa):
    return self.normalize_lineage(taxa)

  def get_ranklist(self):
    return self.ranklist
