#-------------------------------------------------------------------------------
#  File: taxon.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Copyright: 2018 The University of Sydney
#  Version: 0
#  Description: The Taxon class implements a taxonomical unit.
#-------------------------------------------------------------------------------
class BasicNcbiTaxon:

  def __init__(self, taxid=-1, name=None, rank=None, parent_taxid=None):
    self.taxid = int(taxid)
    self.name = name
    self.rank = rank
    self.scientific_name = None
    self.parent_taxid = int(parent_taxid) if parent_taxid != None else parent_taxid
    self.synonyms = []
    self.acronyms = []
    self.isQuery = False
    self.children = {}
    self.division = None
    self.genetic_code_id = None
    self.genetic_code_name = None
    self.mitogenetic_code_id = None
    self.mitogenetic_code_name = None
    self.create_date = None
    self.update_date = None
    self.pubdate = None
    self.aliases = []

  def hasAlias(self):
    if len(self.aliases) > 0:
      return True
    return False

  def get_alias(self):
    if len(self.aliases) > 1:
      sys.exit("Taxonomist::Warning:: More than 1 alias for {}. Check and implement.Abort".format(self.taxid))
    return self.aliases[0]

  def add_synonym(self, synonym):
    self.synonyms.append(synonym)

  def add_acronym(self, acronym):
    self.acronyms.append(acronym)

  def add_alias(self, alias):
    self.aliases.append(int(alias))

  def add_child(self, child):
    self.children[int(child.taxid)] = child

  def dump(self):
    return "taxid: {}\nname: {}\nrank: {}\nparent_taxid: {}\nsynonym: {}\n\
            \racronym: {}\nisQuery: {}\ndivision: {}\ngenetic_code_id: {}\n\
            \rgenetic_code_name: {}\nmitogenetic_code_id: {}\n               \
            \rmitogenetic_code_name: {}\ncreate_date: {}\nupdate_date: {}\n  \
            \rpubdate: {}\naliases: {}\nchildren: {}".format(str(self.taxid),
            self.name, self.rank, self.parent_taxid, self.synonyms,
            self.acronyms, self.isQuery, self.division, self.genetic_code_id,
            self.genetic_code_name, self.mitogenetic_code_id,
            self.mitogenetic_code_name, self.create_date, self.update_date,
            self.pubdate, self.aliases, self.children)
