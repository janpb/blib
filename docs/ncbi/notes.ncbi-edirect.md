# NCBI Edirect Notes

## mode/format compatibility list

  | format | mode |
  |--------|-----:|
  | uilist | text |
  | docsum | xml  |
  |        | json |


## WebEnv and QueryKey

- Need to test if understand this correctly:

    - WebEnv: represent a sessions id, i.e. the first query initiates a WebEnv
    - QueryKey: The UID set of the current WebEnv session
    - Example:

        0. **Epost**
            -  Starts session with `UID_set1`  
            -  Returns: WebEnv `wenv` with `query_key 1`
        1. **Esearch**
            -  Search `UID_set1` using WebEnv `wenv` and `query_key 1`
            -  Returns: new UID set `UID_set2` with new `query_key 2` but old WebEnv 
                        `wenv`        
        2. **Efetch**
            - Fetch `UID_set1` using WebEnv `wenv` and `query_key 1` 
            - Returns: Data for `UID_set1`
            - Fetch `UID_set2` using WebEnv `wenv` and `query_key 2` 
            - Returns: Data for `UID_set2`

## NCBI search request
NCBI uses variables numbers for each search:  retstart, retmax and count [0].

| Variable   | Description                                                                                 |
|------------|---------------------------------------------------------------------------------------------|
| `retstart` | sets the first UID to fetch, i.e. skips the first retstart UIDs. `0` based |
| `retmax`   | sets the number of UIDs to fetch, (max 100,000 UIDs per request) |
| `count`    | this number is returned from each search request and depicts the total number of found  UIDs|

These 3 numbers can be used to perform follow up searches based on the first
search.

### Example (2017-08-17):

NCBI search term:
`(("Bacteria"[Organism]) AND genome[title]) AND "Yersinia pestis"[porgn]`
The first request [0]_ returns the values for the 3 variables.

Batch(request) | retstart  | retmax  |  count   | fetched |
--------------:|----------:|--------:|---------:|--------:|
             0 |         0 | 100,000 |  376,490 | 100,000 |
             1 |   100,000 | 100,000 |  376,490 | 200,000 |
             2 |   200,000 | 100,000 |  376,490 | 300,000 |
             3 |   300,000 |  76,490 |  376,490 | 376,490 |

To get all 182523 UIDs, the follow up search needs to start at 100,000 and
retriev 82523 UIDs. Therefore, the new retstart is the sum of previous
`retmax` and `retstart` and the new `retmax` is the difference from `count` and
`retstart`.

[0] https://dataguide.nlm.nih.gov/eutilities/utilities.html#esearch


##Callimachus

Facilitate EDirect requests by preparing single epost, elink, esearcg and efetch requests.

### Current issues
 - return results only or analyzers
 - results only is preferred since several instances
   of the same analyzer are kinda useless.

 - However, Efetch results are not standardized, i.e.
   how does Callimachus knows what I want to do with
   the fetched data? Create an additional virtual
   function to return results?

## Callimachus Tests

**NOTE: Currently, searching and fecthing is tested. More tests will follow**

Testing Callimachus is done by comparing the data sets obtained from NCBI's
Edirect and Callimachus using the same terms. The tests and parameters are
outlined in a JSON file.

## Callimachus tester
Testing is done using `test/src/callimachus_tester.py`. 
Usage:
`callimachus_tester.py [-h] [-e EMAIL] [-t TESTS]

Callimachus Tester

optional arguments:
  -h, --help            show this help message and exit
  -e EMAIL, --email EMAIL
                        Email required by Edirect
  -t TESTS, --tests TESTS
                        Tests in JSON format`

### Esearch
 - Compare the count from a search term

### Efetch
 - Fetch a subset of the previous Esearch data, using more than one request
   for Callimachus to test the retrieval of batches.
 